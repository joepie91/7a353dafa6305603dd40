someObject = instantiateSomePersistentObjectSomehow();

app.use(function(req, res, next) {
	req.someObject = someObject;
	next();
});

// ...

app.get("/sample/route", function(req, res) {
	console.log("We still have our persistent object...", req.someObject);
	// ...
});